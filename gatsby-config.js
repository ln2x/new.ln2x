module.exports = {
  siteMetadata: {
    title: `LN2X`,
    description: `LN2X is a company focused on developing innovative solutions to help address challenges in information security, risk, compliance and learning. LN2X also provides custom advisory services on a range of specialist areas including payment system security.`,
    author: `LN2X`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ln2x`,
        short_name: `ln2x`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `static/assets/images/ln2x.io-dark-multicolour.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-offline`,
  ],
}
