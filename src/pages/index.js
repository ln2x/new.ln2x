import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <section className="fdb-block section parallax bg1">
  <div className="container">
    <div className="row justify-content-center">
      <h1>Welcome to LN2X</h1>
    </div>
    <div className="row justify-content-center">
      <p className="lead" align="center">LN2X is a company focused on developing innovative solutions to help address challenges in information security, risk, compliance and learning. LN2X also provides custom advisory services on a range of specialist areas including payment system security.</p>
    </div>
  </div>
</section>

<section className="fdb-block" data-block-type="features" data-id="18" draggable="true">
  <div className="container">
    <div className="row text-right align-items-center">
      <div className="col-7 col-md-4 m-auto">
        <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_update_uxn2.svg"/>
      </div>
      <div className="col-12 col-md-7 col-lg-5 m-auto text-left pt-5 pt-md-0">
        <div className="row pb-lg-3">
          <div className="col-3">
            <img alt="image" className="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@2.0.1/dist/imgs//icons/layers.svg"/>
          </div>
          <div className="col-9">
            <h3><strong>Control Risk, Security & Compliance</strong></h3>
            <p>Log Controls, Tasks, Risks, Reports, Policies, and make compliance a thing of the past.</p>
          </div>
        </div>
      <div className="row pt-4 pt-md-3 pb-lg-3">
          <div className="col-3">
            <img alt="image" className="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@2.0.1/dist/imgs//icons/monitor.svg"/>
          </div>
          <div className="col-9">
            <h3><strong>Learning Management, KPI's & CSFs</strong></h3>
            <p>Manage your awareness from a single standalone portal (whitelabel) built on lightnight fast technology.</p>
          </div>
        </div>
      <div className="row pt-4 pt-md-3">
          <div className="col-3">
            <img alt="image" className="img-fluid" src="https://cdn.jsdelivr.net/gh/froala/design-blocks@2.0.1/dist/imgs//icons/cloud.svg"/>
          </div>
          <div className="col-9">
            <h3><strong>Leverage your Data</strong></h3>
            <p>Track learner skills against tasks, risks, assets, controls? get real actionable data and automate your reporting. </p>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>



<section>
    <div className="container">
      <div className="row text-center mt-5">
      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-up" data-aos-delay="200">
        <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_choice_9385.svg"/>
        <h2><strong>Control</strong></h2>
        <p className="lead">track compliance, risk and security related activities, deadlines, tasks. Analytics, Reporting, Graphs, Policies, DONE.</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="/control">More info</a></p>
      </div>
      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-up" data-aos-delay="600">
        <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_creative_team_r90h.svg"/>
        <h2><strong>Get Help</strong></h2>
        <p className="lead">Our consulting and advisory services include cyber security, risk management, security, compliance and technology strategy. </p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="/help">More Info</a></p>
      </div>
      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-up" data-aos-delay="1200">
        <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_done_a34v.svg"/>
        <h2><strong>Learn</strong></h2>
        <p className="lead">We create, manage and deliver training. Get ready for learning content and learning management like it should be.</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="/learn">More Info</a></p>
      </div>
    </div>
  </div>
</section>


  </Layout>
)

export default IndexPage
