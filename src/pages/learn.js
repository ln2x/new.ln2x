import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const LearnPage = () => (
  <Layout>
    <SEO title="Learn" />
    <section class="fdb-block" data-block-type="call_to_action" data-id="16" draggable="false">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col">
            <h1 align="center">Training</h1>
            <p class="lead" align="center">We understand training & awareness, especially in complex environments with a multitude of requirements. The LN2X team have delivered face-to-face and eLearning to over 7000 professionals across over 50 countries. We've used and built technical learning content and learning management solutions, and we actively works with some of the worlds largest organisations advising on Learning & Development. Coming soon a completely new LMS module which works with our GRC solution.</p>
          </div>
        </div>
      </div>
    </section>
    <section className="fdb-block" data-block-type="features" data-id="22" draggable="true">
    <div className="container">
      <div className="row text-center mt-5">
        <div className="col-12 col-sm-4" data-aos="fade-right" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_authentication_fsn5.svg"/>
          <h3><strong>Intuitive</strong></h3>
          <p>"Simple interface with clear options. Open browser, log in and start learning "</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0"  data-aos="fade-left" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="55%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_JavaScript_Frameworks_x21l.svg"/>
          <h3><strong>Lightning Fast</strong></h3>
          <p>Built using actual modern technology, HTML5, CSS, JS, LRS</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-down" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_creation_process_vndn.svg"/>
          <h3><strong>Interactive Content</strong></h3>
          <p>Built-in content interaction, rewards, assesments, certificate downloads, notifications</p>
        </div>
      </div>
      <div className="row text-center mt-5">
        <div className="col-12 col-sm-4" data-aos="fade-right" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="55%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_control_panel1_20gm.svg"/>
          <h3><strong>Custom Branding & DNS</strong></h3>
          <p>"Customise layouts, themes, icons, images, logos, favicons and, yes, your own domain and sender identity"</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-up" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_features_overview_jg7a.svg"/>
          <h3><strong>Ready Made Catalogue</strong></h3>
          <p>Over 500 courses from compliance and standards to encryption and application security</p>
        </div>

        <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-left" data-aos-delay="200">
          <img alt="image" className="img-fluid" width="55%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_marketing_v0iu.svg"/>
          <h3><strong>In-built Support </strong></h3>
          <p>Our support team is ready to help with setup, design, configuration or even complete custom projects.</p>
        </div>
      </div>

    <div className="row text-center mt-5">
      <div className="col-12 col-sm-4" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid" width="65%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_grades_j6jn.svg"/>
        <h3><strong>Advanced Analytics & Reporting</strong></h3>
        <p>"Report on progress down to interaction/question/answer level, easily create benchline KPIs and CSFs"</p>
      </div>

      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-right" data-aos-delay="200">
        <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_browser_stats_704t.svg"/>
        <h3><strong>Segmentation, Groups & Cohorts</strong></h3>
        <p>Easily create groups and manage learning paths. Configure suggestions based on user preferences and interests</p>
      </div>

      <div className="col-12 col-sm-4 pt-4 pt-sm-0" data-aos="fade-left" data-aos-delay="200">
        <img alt="image" className="img-fluid" width="75%" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_organize_resume_utk5.svg"/>
        <h3><strong>Policy Acknowledgement</strong></h3>
        <p>Include your own policies, assign to users for completion. Easily comply with all PCI DSS confirmation requirements; infosec policy signoff, key custodian acknowledgement...</p>

      </div>
    </div>
    </div>
  </section>

  <section className="fdb-block" data-block-type="features" data-id="24" draggable="false">
      <div className="container">
        <div className="row text-center">
        <div className="col-4">
        <h2>Login</h2>
        <p className="lead">LN2X Learning Management Module Existing Customers</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="https://learn.ln2x.io" target="_blank">Login</a></p>
        </div>
        <div className="col-4">
        <img alt="image" className="img-fluid" src="https://s3-us-west-1.amazonaws.com/ln2x.io/PBLLN2X/undraw_experts3_3njd.svg"/>
        </div>
        <div className="col-4">
        <h2>Demo</h2>
        <p className="lead">Still struggling your solution? contact us to arrange a demo.</p>
        <p><a className="btn btn-primary mt-4 mb-5 mb-md-0" href="https://calendly.com/ln2x" target="_blank">Schedule a Call</a></p>
        </div>
      </div>
    </div>
  </section>
  </Layout>
)

export default LearnPage;
