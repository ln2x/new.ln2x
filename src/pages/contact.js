import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

class ContactPage extends React.Component{
  componentDidMount() {
    const head = document.querySelector('head');
    const script = document.createElement('script');
    script.setAttribute('src',  'https://assets.calendly.com/assets/external/widget.js');
    head.appendChild(script);
  }
render(){return(
  <Layout>
    <SEO title="Contact" />
    <section className="fdb-block" data-block-type="features" data-id="22" draggable="false">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-12 text-center">
        <h1>Contact Us</h1>
        <p className="lead">Whatever your question we will answer, please do ask.</p>
      </div>
    </div>
  </div>
</section>

<section className="fdb-block pt-0" data-block-type="contacts" data-id="17" draggable="false">
  <div className="container">
    <div className="row mt-5 text-center">
      <div className="col-12 col-md-6 col-lg-5 text-center">
        <h2>Call or Email</h2>

        <p className="lead"><strong>Enquiries</strong><br/> info@ln2x.io</p>
        <p className="lead"><strong>Helpline EMEA</strong><br/>+44 2033229095</p>
        <p className="lead"><strong>Helpline LATAM</strong><br/>+52 8141707161</p>
        <h2>LN2X Group</h2>
        <p className="lead">HQ/Reg<br/>Ashford, Kent</p>
        <p className="lead">TN24 8LF, United Kingdom</p>
      </div>
      <div className="col-12 col-md-6 col-lg-5 text-center">
      <h2>Schedule it</h2>
      <p className="lead">Use our handy calendar system :) </p>
      <div className="calendly-inline-widget" data-url="https://calendly.com/ln2x?hide_landing_page_details=1" style={{minWidth:320,height:580}}></div>
      <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
    </div>
    </div>
  </div>
</section>
  </Layout>
)}}

export default ContactPage;
